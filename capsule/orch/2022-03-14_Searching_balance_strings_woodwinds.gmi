## Searching for balance with Strings and Woodwinds

Monday March 14th, 2022

I managed to go through the course on Woodwind orchestration a couple of months ago or so. It was a great course that had the same structure as the course on String orchestration (which I took prior to this one) as well as building on top of that course too.

=> https://www.udemy.com/course/orchestrationcourse2/ Woodwind orchestration course

The course ends with a project: I have to compose a piece of music and orchestrate it with Woodwind and String instruments. This is the second project of this kind that I'm undertaking and it is what I've been trying to do on and off for the past few weeks when I was able to make time for it.

I want to be a bit more ambitious with this one: I'd like to compose a short rondo, going for the "ABACA" pattern. I'll give more details about this choice in a future post when I manage to complete this composition (hopefully). I also got a theme down. That required me to use a piano keyboard over three small sessions to explore and refine some ideas (moving a few notes around in MuseScore helped me too). What I'm struggling with is what blocks me to go beyond the blank page.

=> https://en.wikipedia.org/wiki/Rondo Rondo form on Wikipedia

### Which instruments to combine together?

When writing a piece for Strings, I had to consider 5 voices: Violin I, Violin II, Viola, Cello and Contrabass. I was able to (somewhat) wrap my head around which role I wanted to assign to each instrument, at different point of time in the piece. For this second project, I'm also adding the following Woodwinds on top of the five Strings I just mentioned: Flutes, Oboes, Bb Clarinets and Bassoons. Because of a higher number of instruments having different properties, balance is now more difficult to reach. It's about trying to find the right instrument combinaisons. As the course teacher wrote in one of her slides: Think of combining instruments and colours as cooking".

Luckily, there are plenty of resources out there, and I managed to find the following ones:

### Vienna Symphonic Library - Instrumentology section

I first heard about the Vienna Symphonic Library (VSL) because of the samples they sell on their websites. I just found out now about their Instrumentology section which looks fantastic! The first paragraph on this page says:

"The idea behind our Vienna Academy was to provide composers and arrangers not only with the most comprehensive store of virtual orchestral instruments but also with detailed information on what these instruments can do. Whether you are composing or orchestrating for real or virtual instruments, the possibilities and limitations are roughly the same. In both cases, it is essential to understand their specific sound characteristics and playing techniques across the instrument’s pitch and dynamic range. A good orchestrator/composer also knows how to combine instruments within a section and in larger ensembles to get the best sonic results."

This sounds like a perfect companion to the orchestration course I took! As mentioned in the last sentence of this paragraph, this VSL section provides information on different possible combinaisons. See the Woodwinds and the Strings sections for more details on that topic.

=> https://www.vsl.co.at/en/Academy/Instrumentology Instrumentology on VSL
=> https://www.vsl.co.at/en/Instrumentology/Woodwinds Woodwinds section in VSL Instrumentology
=> https://www.vsl.co.at/en/Instrumentology/Strings Strings section in VSL Instrumentology

### Andrew Hugill's website - Combinaisons

I also stumbled upon this website that provides combinaison samples of existing pieces for orchestra. It's very useful to be able to listen to random excerpts (or "recipes") to have a quick idea about what to expect from some combinaisons.

=> https://andrewhugill.com/OrchestraManual/section_combinations_intwos.html Andrew Huggill's website

=> gemini://daima.fr/orch/ (back to 🎼 Music orchestration)
=> gemini://daima.fr (back to homepage)
