## Publish content with git

If your Gemini capsule is version-controlled using 'git', these few steps will allow you to use 'git' to also publish your changes made locally onto your server.

### Create a git bare repository on your server

```
mkdir ~/repo && cd ~/repo
mkdir gemini.git && cd gemini.git
git init --bare
```

### Create a post-receive hook (based on provided template)

```
cd ~/repo/gemini.git/hooks
wget -O post-receive https://codeberg.org/etienne/gemini/raw/branch/main/post-receive-template
chmod +x post-receive
```

Once downloaded, update 'post-receive':
* Replace <username> with your username used to access your Gemini capsule.
* Set the GEMINI_CONTENT_SOURCE value with the Gemini content root folder (not necessarily your project root folder)

### Add your new repo as remote, then publish

From your project root folder, on your machine:

```
git remote add gemini username@your.server.ip.address:~/repo/gemini.git
```

You're all done. You can now publish your content via 'git' by running:

```
git push gemini master
```

=> gemini://daima.fr/tech/ (back to 💻 Tech)
=> gemini://daima.fr (back to homepage)
