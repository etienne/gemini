# Notes sur Gemini

## Installation d'un serveur Agate

1. Installer le serveur Agate : https://github.com/mbrubeck/agate
   - Plusieurs façons d'installer le serveur. En voici deux:
     - Soit en [téléchargeant le binaire](https://github.com/mbrubeck/agate/releases) dans un dossier spécifique :
       ```bash
       mkdir ~/bin
       # Ajuster l'architecture et la version si besoin.
       wget -O ~/bin/agate.gz https://github.com/mbrubeck/agate/releases/download/v2.5.2/agate.x86_64-unknown-linux-gnu.gz 
       cd ~/bin && gunzip agate.gz
       chmod +x agate
       ```
     - Soit en utilisant `cargo` (et en installant la Rust toolchain au préalable) :
       ```bash
       curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
       cargo install agate
       ```
       Possible erreur si installation sur un nouveau serveur Debian : `error: linker 'cc' not found`. Résolution : `sudo apt install build-essential`

1. Ajouter du contenu
   - Créer un dossier qui contiendra du contenu à publier:
     ```bash
     mkdir ~/gemini && cd ~/gemini
     echo "# Hello World" > index.gmi
     ```

1. Installer un certificat TLS et une clé privée
   - Choisir/créer un dossier où seront stockés le certificat et la clé :
     ```bash
     mkdir -p ~/certs/gemini && cd ~/certs/gemini
     # Remplacer nom.de.domaine.com ci-dessous par le nom de domaine choisi.
     openssl req -new -subj "/CN=nom.de.domaine.com" -x509 -newkey ec -pkeyopt ec_paramgen_curve:prime256v1 -days 3650 -nodes -keyout key.pem -out cert.pem 
     ```

1. Ajouter Agate à `systemd`
   - Ajouter un nouveau service, en se basant sur [ce template](./agate-template.service) :
     ```bash
     cd /etc/systemd/system
     sudo wget -O agate.service https://codeberg.org/etienne/gemini/raw/branch/main/agate-template.service
     ```
   - Dans le template téléchargé, remplacer `<username>` et `<nom.de.domaine.com>` par le nom d'utilisateur utilisé jusqu'ici et le nome de domaine choisi.

1. Lancer le service Agate
   ```
   sudo systemctl start agate
   sudo systemctl enable agate
   ```
   Et c'est bon ! Essayer de contacter le site via un navigateur Gemini, comme [Lagrage](https://gmi.skyjake.fi/lagrange/) par exemple.

   S'il y a une erreur de connexion :
   - s'assurer que le port 1965 est accepté au niveau du firewall.
   - exécuter `sudo journalctl -f` pour vérifier que le service agate ne renvoie
     pas d'erreur.

---

Sources qui ont permis l'élaboration de ce guide d'installation :
- [Projet Agate sur GitHub - readme](https://github.com/mbrubeck/agate#installation-and-setup)
- [Billet sur bortzmeyer.org](https://www.bortzmeyer.org/gemini.html)
- [Vidéo de Chris Were](https://share.tube/videos/watch/4fe4e1f0-7896-4b8c-bfb8-2ff19c78d8e5)

## Déployer du contenu sur une capsule Gemini via Git

   Ces quelques étapes permettent de mettre en place un dépôt vide sur le serveur où se trouve une capsule Gemini. Il sera alors possible d'y pousser via `git` un projet Gemini (statique) créé localement afin de le publier automatiquement.

1. Créer un dépôt vide sur serveur, qui sera la cible des `git push` fait
   localement.

   ```bash
   mkdir ~/repo && cd ~/repo
   mkdir gemini.git && cd gemini.git
   git init --bare
   ```

1. Créer un hook `post-receive`, basé sur [ce
   template-là](./post-receive-template).

   ```bash
   cd ~/repo/gemini.git/hooks
   wget -O post-receive https://codeberg.org/etienne/gemini/raw/branch/main/post-receive-template
   chmod +x post-receive
   ```

1. Modifier le `post-receive` script :
   - Remplacer `<username>` par le nom d'utilisateur utilisé pour acceder au contenu Gemini.
   - Ajuster la valeur de `GEMINI_CONTENT_SOURCE` si le contenu à publier se trouve dans un
   dossier spécifique de votre projet.

1. De retour sur le localhost, depuis la racine du projet,
   rajouter le nouveau dépôt vide en tant que `remote`. Par example :

   ```bash
   git remote add gemini username@adresse.ou.ip.serveur:~/repo/gemini.git
   ```

1. Et voilå ! Vous pouvez maintenant publier votre contenu depuis votre
   localhost via `git` en faisant:

   ```bash
   git push gemini master
   ```
